<?php
namespace common;
require_once __DIR__.'/../autoload.php';

use \think\Log as ThinkLog;

class Log
{

    public static $config = [
        'level'=>['emergency','alert','critical','error','warning','notice','info','debug','sql'],
        'path'=>'logs/'
    ];

    public static $logObj = null;

    public static function record($msg, $type = 'info', array $context = []){
        if(is_null(self::$logObj)){
            $log = new ThinkLog();
            self::$logObj = $log->init(self::$config);
        }
        self::$logObj->record($msg,$type,$context);
        if(isset(self::$config['level']) && in_array('debug',self::$config['level'])){
            echo $msg.PHP_EOL;
        }
    }
}
