<?php
namespace common;
require_once __DIR__.'/../autoload.php';

use Workerman\Worker;
use Exception;

class BaseWorker extends Worker 
{

    public function __call($name,$args)
    {
        if(isset($this->$name)){
            call_user_func($this->$name,...$args);
        }else{
            throw new Exception('call to undefined method '.$name);
        }
    }
}
