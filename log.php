<?php
require_once __DIR__.'/autoload.php';
use workerman\Worker;
use common\Log;

$log = new Worker('udp://0.0.0.0:6666');
$log->onMessage = function($connection,$data){
    $msg = "send ip is {$connection->getRemoteIp()},port is {$connection->getRemotePort()}";
    Log::$config['path'] = 'llogs/';
    Log::record($msg);
    // $connection->send($msg);
};

Worker::runAll();